package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
    	System.out.println("Let's play round " + roundCounter);
    	roundCounter +=1;
    	
    	String humanChoice = userChoice();
    	String pcChoice = randomChoice();
    	System.out.print("Human chose " + humanChoice + " computer chose " + pcChoice + ". "  );
    	
    	// The play sequence of the game
    	if (winner(humanChoice, pcChoice)) {
    		System.out.println("Human wins!");
    		humanScore +=1;
    	}
    	else if (winner(pcChoice, humanChoice)) {
    		System.out.println("PC wins!");
    		computerScore +=1;
    	}
    	else {
    		System.out.println("It's a draw!");
    	}
    	System.out.println("Score: human " + humanScore + ", computer " + computerScore); 	
    	String playMore = continuePlaying();
    	
    	if (playMore.equals("y")) {
    		run();
    	}
    	else {
    		System.out.println("Bye bye :)");
    	}	
    	
    }
    
    public String continuePlaying() {
    	// Check if a player want to play more, or not 
    	while (true) {
    		String answer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
    		if (validation(answer, Arrays.asList("y", "n"))) {
    			return answer;
    		}
    		else {
    			System.out.println("Pleas anter a valid answer (y, n)");
    		}
    	}
    }
    
    public String userChoice() {
    	//Make sure the users choice is valid, and return a valid answer to the run module
    	while (true) {
	    	String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
	    	if (validation(humanChoice, rpsChoices)) {
	    		return humanChoice;
	    	}
	    	else {
	    		System.out.println("I do not understand cardboard. Could you try again?");
	    	}
    	}
    }
    
    public boolean validation(String input, List<String>list) {
    	// Check if a input is valid or not
    	boolean isValid = list.contains(input);
    	return isValid;
    }
    
    public String randomChoice() {
    	// Generate a random choice for the PC player
    	Double randomNumber = Math.random()*3;
    	int number = randomNumber.intValue();
    	return rpsChoices.get(number);
    }
    
    public boolean winner(String choice1, String choice2) {
    	// Checks if the human win over the PC, and return True, or the other way and return false. 
    	if (choice1.equals("paper")) {
    		return (choice2.equals("rock"));
    	}
    	else if (choice1.equals("scissors")) {
    		return (choice2.equals("paper"));
    	}
    	else {
    		return (choice2.equals("scissors"));
    	}
    }
    	
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
}
